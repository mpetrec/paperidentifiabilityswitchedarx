#
import switched_systems
from numpy import *
import ConfigParser


def poly_coprime(poly1,poly2):
	roots1 = roots(poly1)
	roots2 = roots(poly2)

	is_coprime = True
	for x in roots1:
		for y in roots2:
			if (abs(x-y) < 1e-6):
				is_coprime=False
				print "Common root of :"+str(poly1)+" and "+str(poly2)+" is "+str(x)+"\n"
				return is_coprime

	return is_coprime
	
	


def GenerateAMatrix(output_reg_length, input_reg_length, regression_parameters):
	state_dimension = output_reg_length + input_reg_length

	A = zeros((state_dimension,state_dimension))

	for i in range(state_dimension):
		A[0][i] = regression_parameters[0][i] 
	

	for i in range(output_reg_length-1):
		A[i+1][i]=1 

	for i in range(input_reg_length-1):
		A[output_reg_length+i+1][output_reg_length+i]=1


	return A
	



#SISO SARX
class SARX:
	attribute_list = ["discrete_modes", "output_reg_length", \
                    "input_reg_length","regression_parameters"]


  	def copy(self, sarx):
                for attrib in SARX.attribute_list:
                        self.__dict__[attrib] =  sarx.__dict__[attrib]

	def __init__(self, **arguments):
		if arguments.has_key("copy"):
			SARX.copy(self,\
			  arguments["copy"])
		
		else:
			data = dict()
			if arguments.has_key("config_file"):
				configf = ConfigParser.\
				            ConfigParser()
				
				fp = arguments["config_file"]

				configf.readfp(fp)

				data_list = configf.items(\
				 "SARX")

				for element in data_list:
					key       = element[0]
					value     = element[1]
					data[key] = eval(value)

			else:
			   	data = arguments
				
			
			for key in \
			 		SARX.attribute_list:
			 		self.__dict__[key] = \
				 		 data[key]
				  

	def SARX2LSS(self):
		la_matrices = dict([])
		lb_matrices = dict([])
		lc_matrices = dict([])


		state_dimension = self.input_reg_length + \
			self.output_reg_length 

		for dstate in self.discrete_modes:
			la_matrices[dstate]=GenerateAMatrix(\
                          self.output_reg_length, self.input_reg_length,\
                           self.regression_parameters[dstate]) 

			lc_matrices[dstate] = self.regression_parameters[dstate] 
			lb_matrices[dstate] = zeros((state_dimension,1))

			lb_matrices[dstate][self.output_reg_length][0] = 1 

		
		linitial_states=dict([('1', zeros((state_dimension,1)))])


		linswitch = switched_systems.LinearSwitchedSystem(discrete_modes=self.discrete_modes,\
    a_matrices = la_matrices, b_matrices=lb_matrices, \
    c_matrices=lc_matrices, initial_states=linitial_states)

		return linswitch


	def ownprint(self):
		print "Discrete modes: "+ str(self.discrete_modes) +"\n"
		print "Length of output reg.: "+ str(self.output_reg_length) +"\n"
		print "Length of input reg.: "+ str(self.input_reg_length) +"\n"
		for mode in self.discrete_modes:
			print "Discrete mode: "+ str(mode)+ \
    				" Parameters: "+array2string(self.regression_parameters[mode])+"\n"


	def Chi(self,q):
		result=[1]

		for index in range(self.output_reg_length):
			result.append(-self.regression_parameters[q][0][index])

		return poly1d(result)


	def Psi(self,q1,q2,j=[]):

		if j==[]:
		   j=self.input_reg_length

		n = self.output_reg_length+self.input_reg_length

		if j==0:
			return [(reshape(eye(n)[0],(n,1)), poly1d([1]))]
		
		z=poly1d([1,0])
		old_list = self.Psi(q1,q2,j-1)

		(old_d,old_poly) = old_list[0]
		

		d=zeros((n,1))

		dval0=-dot(reshape(self.regression_parameters[q2],(1,n)),old_d)

		#print "dval0:"+str(dval0)+"\n"
		#print "Oldval:"+str(old_d)+"\n"
		#print "old_poly:"+str(old_poly)+"\n"

		d[0]=dval0[0]

		for index in range(n-1):
			d[index+1]=old_d[index]

#z*old_poly+

		new_poly_free = dot(self.regression_parameters[q2]-self.regression_parameters[q1],old_d)[0][0]
		
		#print "	new_poly_free:"+str(new_poly_free)+"\n"


		new_poly = z*old_poly+poly1d([new_poly_free])

		new_list = [(d,new_poly)]+old_list
		
		return new_list


	def Phi(self,q1,q2):
		poly_list = self.Psi(q1,q2,self.input_reg_length)

		poly0=poly1d([0])
		z = poly1d([1,0])


		for index in range(self.input_reg_length):
			#print "Index: "+str(index)+"\n"
			#print "Poly_list "+str(poly_list[index+1][1])+"\n"
			poly0=poly0+self.regression_parameters[q2][0][(self.input_reg_length-index-1+self.output_reg_length)]*poly_list[index+1][1]		

		return poly0

	def Upsilon1(self,q2):
		poly0=poly1d([0])
		z = poly1d([1,0])

		for index in range(self.output_reg_length):
			poly0=poly0*z+self.regression_parameters[q2][0][index] 
	

		return poly0

	def Gamma(self,q):

		n = self.output_reg_length+self.input_reg_length
		
		e1 = eye(self.output_reg_length)[0]

		d = 1/self.regression_parameters[q][0][n-1]


          #      print "d: "+str(d)+"\n";
                
		poly_list = []
		

		for index in range(self.input_reg_length):
			new_poly_array = eye(self.input_reg_length-index)[0]
			new_poly_array = reshape(new_poly_array, self.input_reg_length-index)

         #               print "New poly arra: "+str(new_poly_array)+"\n"

			new_poly = poly1d([d])*poly1d(new_poly_array)

			for pindex in range(index):
				new_poly = new_poly-\
                                d*poly_list[pindex]*poly1d([self.regression_parameters[q][0][self.output_reg_length+self.input_reg_length-index+pindex]])
					
	#		print "New poly:\n"
        #                print new_poly

			poly_list.append(new_poly)

  
		return poly_list


	def BigUpsilon(self,q1,q2):
		poly1 = self.Upsilon1(q2)
	
		poly_gamma = self.Gamma(q1)


		for cindex in range(self.input_reg_length):
			poly1=poly1+poly1d([self.regression_parameters[q2][0][self.output_reg_length+cindex]])*poly_gamma[cindex]*self.Chi(q1)
		

		return poly1
		

		e1 = eye(self.output_reg_length)[0]


	def testPolynomials(self):
		for dstate in self.discrete_modes:
			pol=self.Chi(dstate)

			print "Chi polynomial for mode "+str(dstate)+"\n"

			print pol


			pol = self.Upsilon1(dstate)

			print "Upsilon polynomial for mode "+str(dstate)+"\n"

			print pol


			#pol_list = self.Gamma(dstate)

			#print "Gamma polynomials for mode "+str(dstate)+"\n"
			#for index in range(len(pol_list)):
			#	print "Index: "+str(index)+": "+ \
			#	 str(pol_list[index])+"\n"


			for dstate1 in self.discrete_modes:
				print "Psi("+str(dstate)+"," +str(dstate1)+")\n"
				pol_list=self.Psi(dstate,dstate1)

				for index in range(len(pol_list)):
					print "Index: "+\
                                         str(index)+": d "+ \
				         str(pol_list[index][0])+\
                                         ": poly. "+str(pol_list[index][1])+"\n"

				print "Phi("+str(dstate)+"," +str(dstate1)+")\n"
				print self.Phi(dstate,dstate1)
				
				print "BigUpsilon("+str(dstate)+"," +str(dstate1)+")\n"

				print self.BigUpsilon(dstate,dstate1)


		condA=False
                condB=False
		for dstate in self.discrete_modes:
			for dstate1 in self.discrete_modes:
				if poly_coprime(self.Chi(dstate),self.Phi(dstate,dstate1)):
					condA=True
					print "Cond A is true for "+str(dstate)+" "+str(dstate1)+"\n"
				
				if poly_coprime(self.Upsilon1(dstate), self.Chi(dstate1)):
					mN=self.output_reg_length+self.input_reg_length
					condB1=(abs(self.regression_parameters[dstate1][0][mN-1]) > 1e-6) 
 					condB2=(abs(self.regression_parameters[dstate1][0][self.output_reg_length-1]*self.regression_parameters[dstate][0][mN-1]-self.regression_parameters[dstate][0][self.output_reg_length-1]*self.regression_parameters[dstate1][0][mN-1]) > 1e-6)
					print "CondB1: "+str(condB1)+" CondB2: " + str(condB2)+"\n"
					condB = condB or (condB1 and condB2)
					print "Cond B is true for "+str(dstate)+" "+str(dstate1)+"\n"
                      

		  
		return (condA and condB)
