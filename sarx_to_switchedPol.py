#
import switched_systems
from numpy import *
import ConfigParser
import os
os.environ['SAGE_ROOT'] = '/usr/share/sagemath'
os.environ['DOT_SAGE']='/home/mpetrec/.sage'
#os.environ['SAGE_SRC'] = 'you can find this path by running os.environ['SAGE_ROOT'] in sage shell 
#os.environ['SAGE_DOC_SRC'] = 'you can find this path by running os.environ['SAGE_ROOT'] in sage shell 
os.environ['SAGE_LOCAL'] = '/usr'
#os.environ['DOT_SAGE'] = 'you can find this path by running os.environ['SAGE_ROOT'] in sage shell

import sage.all
from sage.rings.real_mpfr  import RealField
from sage.rings.rational_field  import RationalField
from sage.matrix.constructor import Matrix as matrix
from sage.matrix.special import zero_matrix
from sage.matrix.special import block_matrix
from sage.combinat.subset import Subsets


	
	


def GenerateAMatrixPoly(output_reg_length, input_reg_length, regression_parameters, ring):
	state_dimension = output_reg_length + input_reg_length

	A = zero_matrix(ring,state_dimension)

	for i in range(state_dimension):
		print "reg. param"+str(regression_parameters[i])+"\n"
		p=regression_parameters[i]
		A[0,i]=p
	

	for i in range(output_reg_length-1):
		A[i+1,i]=1 

	for i in range(input_reg_length-1):
		A[output_reg_length+i+1,output_reg_length+i]=1


	return A
	



#SISO SARX
class SARXPoly:
	attribute_list = ["discrete_modes", "output_reg_length", \
                    "input_reg_length","regression_parameters", "PolyRing"]

  	def copy(self, sarx):
                for attrib in SARXpoly.attribute_list:
                        self.__dict__[attrib] =  sarx.__dict__[attrib]

	def __init__(self, **arguments):
		if arguments.has_key("copy"):
			SARXPoly.copy(self,\
			  arguments["copy"])
		
		else:
			data = dict()
			if arguments.has_key("config_file"):
				configf = ConfigParser.\
				            ConfigParser()
				
				fp = arguments["config_file"]

				configf.readfp(fp)

				data_list = configf.items(\
				 "SARXPoly")

				for element in data_list:
					key       = element[0]
					value     = element[1]
					data[key] = eval(value)

			else:
			   	data = arguments
				
			
			for key in \
			 		SARXPoly.attribute_list:
			 		self.__dict__[key] = \
				 		 data[key]
			self.__dict__['Theta']=self.PolyRing.gens()
  			self.__dict__['z']=self.Theta[len(self.Theta)-1]
				  

	def SARX2LSSMin(self):
		la_matrices = dict([])
		lb_matrices = dict([])
		lc_matrices = dict([])


		state_dimension = self.input_reg_length + \
			self.output_reg_length 

		
		for dstate in self.discrete_modes:
			la_matrices[dstate]=GenerateAMatrixPoly(\
                          self.output_reg_length, self.input_reg_length,\
                           self.regression_parameters[dstate],self.PolyRing) 

			lb_matrices[dstate] = zero_matrix(self.PolyRing,state_dimension,1)

			lb_matrices[dstate][self.output_reg_length,0] = 1 
			print "Self reg: "+str(self.regression_parameters[dstate])+"\n"
			m=matrix(1, state_dimension,self.regression_parameters[dstate])
			lc_matrices[dstate] = m 

		n=self.input_reg_length+self.output_reg_length
	
		for i in range(len(self.discrete_modes)):
			dstate=self.discrete_modes[i]
			if i==0:
				ContrMatrix=lc_matrices[dstate]
			else:
			 	ContrMatrix=block_matrix([[ContrMatrix],[lc_matrices[dstate]]])

		k=0
		for i in range(n-1):
			ContrMatrix1=block_matrix(self.PolyRing,[])
			for j in range(len(self.discrete_modes)):
				dstate=self.discrete_modes[j]
				if j==0:
					ContrMatrix1=ContrMatrix* la_matrices[dstate]
				else:
			 		ContrMatrix1=block_matrix(self.PolyRing,[[ContrMatrix1],[ContrMatrix*la_matrices[dstate]]])
				k=k+1

			ContrMatrix=block_matrix(self.PolyRing,[[ContrMatrix],[ContrMatrix1]])
		
		print "Contr matrix: "+str(ContrMatrix)+"\n"
		S=Subsets(k,state_dimension,submultiset=True)

		p=[]
		for z in S.list():
			indices=[]
			for x in z:
				indices.append(x-1)

			#print "Subset: "+str(indices)+"\n"
			p.append(ContrMatrix[indices,:].det())
			#print "Det: "+str(p)+"\n"
			
		#print "Calculating Ideal \n"
		return self.PolyRing.ideal(p) #groebner_basis()




	def ownprint(self):
		print "Discrete modes: "+ str(self.discrete_modes) +"\n"
		print "Length of output reg.: "+ str(self.output_reg_length) +"\n"
		print "Length of input reg.: "+ str(self.input_reg_length) +"\n"
		for mode in self.discrete_modes:
			print "Discrete mode: "+ str(mode)+ \
    				" Parameters: "+str(self.regression_parameters[mode])+"\n"


	def Chi(self,q):
		result=1;

		for index in range(self.output_reg_length):
			result=result*self.z + (-self.regression_parameters[q][index])

	
		return result


	def PolyFromCoef(self,coeffs):
		result=0

		for element in coeffs:
			result = result*self.z+element

		return result

	def PolyDot(sef,coeffs1,coeffs2):
		result = 0

		for i in range(len(coeffs1)):
			result = result+coeffs1[i]*coeffs2[i]

		return result



	def Psi(self,q1,q2,j=[]):

		if j==[]:
		   j=self.input_reg_length

		n = self.output_reg_length+self.input_reg_length

		if j==0:
			d=[1]

			for i in range(n-1):
				d.append(0)

			return [(d,1)]
		
		#z=poly1d([1,0])
		old_list = self.Psi(q1,q2,j-1)

		(old_d,old_poly) = old_list[0]
		

		d=[]

		for i in range(n):
			d.append(0)

		dval0=-self.PolyDot(self.regression_parameters[q2],old_d)

		print "dval0:"+str(dval0)+"\n"
		#print "Oldval:"+str(old_d)+"\n"
		#print "old_poly:"+str(old_poly)+"\n"

		d[0]=dval0

		for index in range(n-1):
			d[index+1]=old_d[index]

#z*old_poly+

		z=[]
		for i in range(n):
			z.append(self.regression_parameters[q2][i]-self.regression_parameters[q1][i])	

		new_poly_free = self.PolyDot(z,old_d)
		
		#print "	new_poly_free:"+str(new_poly_free)+"\n"


		new_poly = self.z*old_poly+new_poly_free

		new_list = [(d,new_poly)]+old_list
		
		return new_list


	def Phi(self,q1,q2):
		poly_list = self.Psi(q1,q2,self.input_reg_length)

		poly0=0


		for index in range(self.input_reg_length):
			#print "Index: "+str(index)+"\n"
			#print "Poly_list "+str(poly_list[index+1][1])+"\n"
			poly0=poly0+self.regression_parameters[q2][(self.input_reg_length-index-1+self.output_reg_length)]*poly_list[index+1][1]		

		return poly0

	def Upsilon1(self,q2):
		poly0=0

		for index in range(self.output_reg_length):
			poly0=poly0*self.z+self.regression_parameters[q2][index] 
	

		return poly0

      
	def Gamma(self,q):

		n = self.output_reg_length+self.input_reg_length
		
		e1 = eye(self.output_reg_length)[0]

		d = 1/self.regression_parameters[q][n-1]


          #      print "d: "+str(d)+"\n";
                
		poly_list = []
		

		for index in range(self.input_reg_length):
			new_poly_array = eye(self.input_reg_length-index)[0]
			new_poly_array = reshape(new_poly_array, self.input_reg_length-index)

         #               print "New poly arra: "+str(new_poly_array)+"\n"

			new_poly = d*new_poly_array

			for pindex in range(index):
				new_poly = new_poly-\
                                d*poly_list[pindex]*self.regression_parameters[q][self.output_reg_length+self.input_reg_length-index+pindex]
					
	#		print "New poly:\n"
        #                print new_poly

			poly_list.append(new_poly)

  
		return poly_list


	def BigUpsilon(self,q1,q2):
		poly1 = self.Upsilon1(q2)
	
		poly_gamma = self.Gamma(q1)


		for cindex in range(self.input_reg_length):
			poly1=poly1+self.regression_parameters[q2][self.output_reg_length+cindex]*poly_gamma[cindex]*self.Chi(q1)
		

		return poly1
		

		e1 = eye(self.output_reg_length)[0]


	def testPolynomials(self):
		for dstate in self.discrete_modes:
			pol=self.Chi(dstate)

			print "Chi polynomial for mode "+str(dstate)+"\n"

			print pol


			pol = self.Upsilon1(dstate)

			print "Upsilon polynomial for mode "+str(dstate)+"\n"

			print pol


#			pol_list = self.Gamma(dstate)
#
#			print "Gamma polynomials for mode "+str(dstate)+"\n"
#			for index in range(len(pol_list)):
#				print "Index: "+str(index)+": "+ \
#				 str(pol_list[index])+"\n"


			for dstate1 in self.discrete_modes:
				print "Psi("+str(dstate)+"," +str(dstate1)+")\n"
				pol_list=self.Psi(dstate,dstate1)

				#for index in range(len(pol_list)):
					#print "Index: "+\
                                        # str(index)+": d "+ \
				        # str(pol_list[index][0])+\
                                        # ": poly. "+str(pol_list[index][1])+"\n"
				if dstate != dstate1:

					print "Phi("+str(dstate)+"," +str(dstate1)+")\n"
					print self.Phi(dstate,dstate1)
					
					print "BigUpsilon("+str(dstate)+"," +str(dstate1)+")\n"
		
					print self.BigUpsilon(dstate,dstate1)


		condA=False
                condB=False
		ideal_list1=[]
		ideal_list2=[]
		for dstate in self.discrete_modes:
			for dstate1 in self.discrete_modes:
				if dstate1 != dstate:
					I1=self.PolyRing.ideal([self.Chi(dstate),self.Phi(dstate,dstate1)])
					K1=I1.elimination_ideal(self.Theta[len(self.Theta)-1])
					print "K1: "+str(K1.groebner_basis())+"\n"
					
					
		
					I2=self.PolyRing.ideal([self.Upsilon1(dstate), self.Chi(dstate1)])
					K2=I2.elimination_ideal(self.Theta[len(self.Theta)-1])
					mN=self.output_reg_length+self.input_reg_length
					K3=self.PolyRing.ideal(self.regression_parameters[dstate1][mN-1])
 					K4=self.PolyRing.ideal(self.regression_parameters[dstate1][self.output_reg_length-1]*self.regression_parameters[dstate][mN-1]-self.regression_parameters[dstate][self.output_reg_length-1]*self.regression_parameters[dstate1][mN-1])
					print " dstate1: " + str(dstate1)+" " +str(self.regression_parameters[dstate1][self.output_reg_length-1])+"\n"
					print " dstate1: " + str(dstate1)+" "+ str(self.regression_parameters[dstate1][mN-1])+"\n"
					print " dstate: " + str(dstate)+" "+ str(self.regression_parameters[dstate][self.output_reg_length-1])+"\n"
					print " dstate: " + str(dstate)+" "+ str(self.regression_parameters[dstate][mN-1])+"\n"
					K5=K4*K3
					print "K3 : \n"+str(K3)+"\n K4:\n "+str(K4)+"\n K5:\n "+str(K5)+"\n"
					K6=K5*K2
					print "K6 Groeb: "+str(K6.groebner_basis())+"\n"
                      			ideal_list1=self.PolyRing.ideal(ideal_list1+K1.groebner_basis()).groebner_basis()
					print "Ideal list1: "+str(ideal_list1)+"\n"
					ideal_list2=self.PolyRing.ideal(ideal_list2+K6.groebner_basis()).groebner_basis()
					print "Ideal list2: "+str(ideal_list2)+"\n"
				
		print "Ideal list: "+str(ideal_list1)+"\n"
		print "Ideal list: "+str(ideal_list2)+"\n"
		KR1=self.PolyRing.ideal(ideal_list1)
		print "KR1: "+str(KR1)+"\n"
		KR2=self.PolyRing.ideal(ideal_list2)
		print "KR2: "+str(KR2)+"\n"
		KR=KR1*KR2
		return KR.groebner_basis()
