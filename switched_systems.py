#from  form_pow_repr import *
import form_pow_repr
from dfa_automata import *
from own_io import own_print
from  numpy import *
import hankel_matrix
import ConfigParser
from scipy import integrate

def_precsion = 1e-10
hankel_matrix.def_precision = def_precision

form_pow_repr.def_precision = def_precision 

def init_module(l_def_precision = 1e-10 ):
	
        def_precision = l_def_precision
	hankel_matrix.def_precision = l_def_precision

	form_pow_repr.def_precision = l_def_precision 
	return 	

def HankelMatrixToLinSwitchConst(hankel_matrix, input_dimension,\
                            output_dimension, discrete_modes, \
			    index_set, length, automata):
	linsys = HankelMatrixToLinSwitch( hankel_matrix,\
	                     input_dimension, output_dimension,\
			     discrete_modes, index_set, length)
	
	linsys_const =  LinearSwitchedSystemConst(\
	                     linswitch = linsys, \
			     automata = automata )

	return linsys_const

def HankelMatrixToLinSwitch(hmatrix, input_dimension,\
                            output_dimension, discrete_modes, \
			    index_set, length):
	
	index = []
	for element in index_set:
		index.append(element)
	
	for dstate in discrete_modes:
		for i in range( input_dimension ):
			index.append((dstate, i))
			
	odimension = output_dimension * len(discrete_modes)
	hankm = hankel_matrix.HankelMatrix(\
	                     discrete_modes, odimension,\
	                     hmatrix, index, length)
	
	repr = hankm.ComputeRepresentation()

	linsys = ReprToLinSwitchSys(repr, input_dimension,\
	                                  output_dimension)
	
	return linsys
	

def ReprToLinSwitchSys(repr, input_dimension,\
                         output_dimension ):
	discrete_modes = repr.alphabet
	a_matrices = repr.transition
	c_matrices = dict()
	b_matrices = dict()

	zeta = repr.zeta.copy()
	start_index = 0
	for key in discrete_modes:
		is_empty = True
		for i in range(input_dimension):
			if is_empty:
				bmatrix =reshape(zeta.pop((key,i)),(repr.dimension,1))
				
				is_empty =False
			else:
				bmatrix = concatenate((bmatrix,\
					zeta.pop((key,i))),1)
					
		b_matrices[key] = bmatrix
		end_index = start_index+\
		  output_dimension
		c_matrices[key] = repr.output[start_index:end_index,:]
		start_index=end_index

	linswitch= LinearSwitchedSystem(discrete_modes=\
	   discrete_modes,\
	              a_matrices=a_matrices, \
		      b_matrices=b_matrices ,\
		      c_matrices=c_matrices,\
		      initial_states = zeta)
		      
	return linswitch

class LinearSwitchedSystem:

	attribute_list = ["discrete_modes", \
	                  "a_matrices", "b_matrices", \
			   "c_matrices", "initial_states", \
			   "state_dimension", "input_dimension",\
			   "output_dimension", "initial_states_keys"]
	
	constructor_attribute_list = ["discrete_modes", \
	                   "a_matrices", "b_matrices", \
			   "c_matrices", "initial_states" ]
			   
	def check_object(self):
		return


	def copy(self, linswitch):
		for attrib in LinearSwitchedSystem.attribute_list:
			self.__dict__[attrib] =  linswitch.__dict__[attrib]
	
	def __init__(self, **arguments):
	         #discrete_modes, a_matrices, b_matrices, \
                 #c_matrices, initial_states, is_check=True):
		if arguments.has_key("copy"):
			LinearSwitchedSystem.copy(self,\
			  arguments["copy"])
		
		else:
			data = dict()
			if arguments.has_key("config_file"):
				configf = ConfigParser.\
				            ConfigParser()
				
				fp = arguments["config_file"]

				configf.readfp(fp)

				data_list = configf.items(\
				 "LinearSwitchedSystem")

				for element in data_list:
					key       = element[0]
					value     = element[1]
					data[key] = eval(value)

			else:
			   	data = arguments
				
			
			for key in \
			 		LinearSwitchedSystem.constructor_attribute_list:
			 		self.__dict__[key] = \
				 		 data[key]
				  
			self.state_dimension = self.a_matrices[self.discrete_modes[0]].shape[0]
			self.input_dimension = self.b_matrices[self.discrete_modes[0]].shape[1]
			self.output_dimension = self.c_matrices[self.discrete_modes[0]].shape[0]

			self.initial_states_keys = self.initial_states.keys()

		
		print "State dimension "+ \
		str(self.state_dimension)+\
		" output dimension "+ \
		str(self.output_dimension)+\
		" input dimension "+ \
		str(self.input_dimension)+"\n"
		#if is_check:
		#	self.check_object()
		
		
		
	def ownprint(self, oprecision=10, supp_small=1):
		print "Discrete modes:"+ str(self.discrete_modes) +"\n"
		for mode in self.discrete_modes:
			print "Discrete mode: "+ \
				str(mode) + "\n "+ \
				"A matrix:\n "+ \
				array2string(self.a_matrices[mode],\
				precision = oprecision, \
				suppress_small=supp_small)\
				+"\n "+\
				"B matrix:\n "+\
				array2string(self.b_matrices[mode],\
				precision = oprecision, \
				suppress_small=supp_small)\
				+"\n "+\
				"C matrix:\n "+\
				array2string(self.c_matrices[mode],\
				precision = oprecision, \
				suppress_small=supp_small)\
				+"\n"
		for (key,value) in self.initial_states.iteritems(): 
			print "Initial state: "+ \
			array2string(value,\
				precision = oprecision, \
				suppress_small=supp_small)\
			        +"\n"
	def ComputeIndexList(self, index_set):
		ret_index_set = []
		for key in index_set:
			ret_index_set.append(key)
			
		for state in self.discrete_modes:
			for i in range(self.input_dimension):
				ret_index_set.append((state,i))
		
		return ret_index_set
		
	def ComputeRepresentation(self):
		is_empty = True


		if self.__dict__.has_key("repr"):
			return self.repr
		


		zeta = dict()
		for (key, value) in self.initial_states.iteritems():
			zeta[key] = reshape(\
			   self.initial_states[key],\
			   (self.state_dimension,1))
			        
		for mode in self.discrete_modes:
			if is_empty:
				output=reshape(\
				  self.c_matrices[mode], \
				  (self.output_dimension,\
				   self.state_dimension))
				is_empty = False
			else:
			  	output=concatenate((output, \
				  self.c_matrices[mode]),0)
		
		        for i in range(self.input_dimension):
				zeta[(mode,i)]=reshape(self.b_matrices[mode][:,i], (self.state_dimension,1))
				#print "ZetaB "+\
				#array2string(zeta[(mode,i)])+\
				#" index "+str((mode,i))+"\n"
				
				
			
	        #print "Output: " + str(output)+"\n"		
		self.repr = form_pow_repr.Representation(self.discrete_modes, \
	            self.a_matrices, output, zeta )
	
		return self.repr
		
	def ReachableSystem(self):
		repr  = self.ComputeRepresentation()
		rrepr = repr.ReachableRepresentation()
		return ReprToLinSwitchSys( rrepr,self.input_dimension, self.output_dimension )

	def ObservableSystem(self):
		repr  = self.ComputeRepresentation()
		orepr = repr.ObservableRepresentation()
		return ReprToLinSwitchSys( orepr, self.input_dimension, self.output_dimension )
	
	def MinimalSystem(self):
		repr  = self.ComputeRepresentation()
		mrepr = repr.MinimalRepresentation()
		return ReprToLinSwitchSys( mrepr, self.input_dimension, self.output_dimension )

 	def IsObservable(self):
		repr = self.ComputeRepresentation()
		return repr.IsObservable()

	def IsReachable(self):
		repr = self.ComputeRepresentation()
		return repr.IsReachable()

	def HankelMatrix(self,size=[], rank=[], index_list=[]):
		repr = self.ComputeRepresentation()
		if index_list == []:
			index_list = self.initial_states_keys

		ind_list_repr = self.ComputeIndexList(index_list)
		(hnkm, hkmsize, hnkm_list) = repr.HankelMatrix(\
		                             size, rank, ind_list_repr)
		ind_list = []
		for key in hnkm_list:
			if key in self.initial_states_keys:
				ind_list.append(key)
		
		return (hnkm, hkmsize, ind_list)
	 	
	def MarkovParameterList(self, up_to):
		repr = LinearSwitchedSystem.ComputeRepresentation(self)
		index_list_repr = self.ComputeIndexList(self.initial_states_keys)
		
		wordset = WordGenerate( repr.alphabet, up_to )

		markov_list = dict([])

		for index in index_list_repr:
			count = 0
			for mode in repr.alphabet:
				markov_list[(index,mode)]=[]
				for word  in wordset:
					markov_parameter_list=[]
					for cindex in range(self.output_dimension):
						markov_parameter_list.append(repr.ComputeMarkovParameter(word,index,self.output_dimension * count + cindex))
					markov_list[(index,mode)].append((word,array(markov_parameter_list, float)))
				count = count + 1
					
		return markov_list

	
	def FirstPartialReal(self):
		repr = self.ComputeRepresentation()
		(hank,num,hnkm_list) = repr.FirstPartialReal()
		ind_list = []
                for key in hnkm_list:
                        if key in self.initial_states_keys:
                                ind_list.append(key)	

		return (hank,num,ind_list)

	def SubHankelMatrix(self, size_c,size_r):
		repr = self.ComputeRepresentation()
		return repr.SubHankelMatrix(size_c,size_r)


	def OldHankelMatrix(self,size=[], rank=[], index_list=[]):
		repr = self.ComputeRepresentation()
		if index_list == []:
			index_list = self.initial_states_keys

		ind_list_repr = self.ComputeIndexList(index_list)
		(hnkm, hkmsize, hnkm_list) = repr.HankelMatrix(\
		                             size, rank, ind_list_repr)
		ind_list = []
		for key in hnkm_list:
			if key in self.initial_states_keys:
				ind_list.append(key)
		
		return (hnkm, hkmsize, ind_list)


	def RightHandSide(self, time, cont_state, discrete_state,input):
		a_matrix = self.a_matrices[discrete_state] #.astype('d')
		b_matrix = self.b_matrices[discrete_state]
		#print "A matrix: " + array2string(a_matrix)+"\n"
		#print "B matrix: " + array2string(b_matrix)+\
		#" shape : " + str(b_matrix.shape)+ "\n"

        	#print "C_state: "+str(cont_state)+"\n"

		cont_state=reshape(cont_state, (self.state_dimension,1))
		
	        derivative = matrixmultiply(a_matrix, cont_state)+ \
		   matrixmultiply(b_matrix, input(time))
		   
		#print "Time: " + str(time)+ "input: "+\
		#  str(input(time))+"\n"
		#print "Derivative: "+str(derivative)+"\n"   
		return derivative
		#return reshape(derivative,(cont_state_size,))   
			
	def GenerateStateTrajectory(self, c_state, \
	                         switching, cont_input):
		gtime = 0
		#step_number = 1000
		step_size=0.01
		atol = 1e-8
		rtol = 1e-8
		nsteps = 1000
		method='adams'
		traject = []
                c_state=reshape(c_state, (self.state_dimension,1))
		for (discrete_state,time) in switching:
			def rhside(stime,istate):
			#	print "Init: "+str(istate)+"\n"
				return self.RightHandSide(stime, istate, discrete_state, cont_input)
				
			solver= integrate.ode(rhside).set_integrator('vode',rtol=rtol, atol=atol, nsteps=nsteps, method=method).set_initial_value(c_state,gtime)
			traject.append([gtime, (discrete_state,c_state)])
			#print "Time, state"+ str(gtime)+"," + array2string(c_state) + " discrete state" +str(discrete_state)+ "\n"
			while solver.t  < time + gtime and \
			 solver.successful():
			 	c_state = solver.integrate(solver.t+step_size)
				c_state = reshape(c_state,(self.state_dimension,1))#.astype('d')
				traject.append([solver.t,(discrete_state,c_state)])
					
			#c_state = matrixmultiply(self.reset_maps[(input,d_state)], c_state) #.astype('d'))
			#d_state =  \
			 # self.automata.transition[(input, d_state)]
			
			gtime = gtime + time
			  
		#print "Time, state"+ str(gtime)+"," + array2string(c_state) + " discrete state" +str(d_state)+ "\n"
		#def rhside(stime,istate):
		#	#print "Init:"+str(istate)+"stime: "+str(stime)+"type: "+str(istate.typecode())+"\n"
		#	return self.RightHandSide(stime, istate, discrete_state, cont_input)
		#	
		#solver= integrate.ode(rhside).set_integrator('vode',rtol=rtol, atol=atol, nsteps=nsteps, method=method).set_initial_value(c_state,gtime)
		#traject.append([gtime,(d_state,c_state)])
		#while solver.t  < etime + gtime and \
		#solver.successful():
		#	#print "cstate:"+ array2string(c_state) + " discrete state" +str(d_state)+ "\n"
	 	#	c_state = solver.integrate(solver.t+step_size)
		#	c_state = reshape(c_state, \
		#	  (self.state_dimensions[d_state],1))#.astype('d')
		#	traject.append([solver.t,(discrete_state,c_state)])

		#stime = gtime
		#time_axis = [gtime]
		#while stime < gtime + etime:
		#	stime = stime+ step_size
		#	time_axis.append(stime)
				
#		 	#	c_state = solver.integrate(solver.t+step_size)
		#atime_axis=reshape(array(time_axis, 'd'),len(time_axis))	
		#c_state=reshape(c_state, self.state_dimensions[d_state])
		#atime_axis=linspace(gtime, gtime+etime,step_number)
		#solution = integrate.odeint(rhside,c_state, atime_axis)
		#for rindex in range(atime_axis.shape[0]):
		#   	stime = atime_axis[rindex]
		#	c_state = transpose(solution[0][rindex,:])
		#	traject.append([stime, (d_state,c_state)])
		
	     	
		return traject

	def GenerateContOutput(self, c_state, \
	      switching, input ):
	      	state_traject = self.GenerateStateTrajectory(\
		          c_state, switching, input)
		output_traject = []	  
		for traject_point in state_traject:
		        time = traject_point[0]
			dstate = traject_point[1][0]
			cstate = traject_point[1][1]
			output = matrixmultiply(self.c_matrices[dstate], cstate)
			output_traject.append([time,output])
		
		return output_traject

 
class LinearSwitchedSystemConst(LinearSwitchedSystem):
	def __init__(self, \
	    #discrete_modes, \
	    #a_matrices,\
	    #b_matrices, \
	    #c_matrices, initial_states, 
	    # language,
	    **arguments):
		if arguments.has_key("config_file"):
			config_file = arguments["config_file"]
			LinearSwitchedSystem.__init__(self,\
			 	config_file = config_file ) 
			config_file.seek(0)	
			self.automata = LinSwitchDFA(\
			  config_file = config_file )
			return
			
		if arguments.has_key("linswitch"):
			LinearSwitchedSystem.__init__(self, \
			  copy=arguments["linswitch"])
		
		else:
			args = dict()
			if arguments.has_key("linswitch_file"):
				file = arguments["linswitch_file"]
				args["config_file"] = file
			else:	
				for key in LinearSwitchedSystem.\
				    constructor_attribute_list:
			 		args[key] = arguments[key]
				
			LinearSwitchedSystem.__init__(self,\
				*args)
		
		if arguments.has_key("automata"):
			self.automata = arguments["automata"]
			
		elif arguments.has_key("automata_file"):
			self.automata = LinSwitchDFA(\
			 config_file = arguments["automata_file"])
			 
		else:	
			self.automata = LinSwitchDFA(\
			   self.discrete_modes,
		 	   arguments["language"].states, \
			   arguments["language"].transition,\
		           arguments["language"].accepting_states,\
		           arguments["language"].initial_states,\
		  	  self.input_dimension, \
		          self.output_dimension, self.initial_states.keys())
		
			
		
		 
	def ownprint(self):
		LinearSwitchedSystem.ownprint(self)
		print "Automata: \n"
		self.automata.ownprint()

	def ComputeRepresentation(self):
		repr1 = LinearSwitchedSystem.ComputeRepresentation(\
		            self )
		
		repr2 = self.automata.DFA2Representation()

	        #print "Repr1:"+" dimension: " +\
		#str(repr1.dimension )+"\n"
		#repr1.ownprint()
		#print "Repr2:"
		#repr2.ownprint()

		repr = compute_hadamard_product( repr1, repr2 )
		 
		#print "Before returning\n"
		#repr.ownprint()
		return repr
	
	def MinimalSystem( self ):
		repr = self.ComputeRepresentation()
		mrepr = repr.MinimalRepresentation()
		
	
		linsw = ReprToLinSwitchSys( mrepr, \
		         self.input_dimension, \
			 self.output_dimension )
		
		
		
		#print "Linsw:\n"
		#linsw.ownprint()
		linswconst = LinearSwitchedSystemConst(\
				linswitch=linsw,\
				automata=self.automata)
		#linswconst.ownprint()		
		return  linswconst
	
	def HankelMatrix(self,size=[], rank=[], index_list=[]):
		repr = LinearSwitchedSystemConst.ComputeRepresentation(self)

		if index_list == []:
			index_list = self.initial_states_keys

		index_list_repr = self.ComputeIndexList(index_list)
		(hnkm, hnkmsize, hnkm_list) = repr.HankelMatrix(size,\
		                              rank, index_list_repr)
		ret_index_list = []
		for key in hnkm_list:
			if key in self.initial_states_keys:
				ret_index_list.append(key)
		
		return (hnkm, hnkmsize, ret_index_list)


	
		 

	
		
	def OldHankelMatrix(self,size=[], rank=[], index_list=[]):
		repr = LinearSwitchedSystemConst.ComputeRepresentation(self)

		if index_list == []:
			index_list = self.initial_states_keys

		index_list_repr = self.ComputeIndexList(index_list)
		(hnkm, hnkmsize, hnkm_list) = repr.HankelMatrix(size,\
		                              rank, index_list_repr)
		ret_index_list = []
		for key in hnkm_list:
			if key in self.initial_states_keys:
				ret_index_list.append(key)
		
		return (hnkm, hnkmsize, ret_index_list)
