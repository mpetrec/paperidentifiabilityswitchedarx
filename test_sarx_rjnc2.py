#!/usr/bin/env python
from numpy import *
from sets import *
import switched_systems
import sys,os
import copy
import Gnuplot
import sarx_to_switched
from numpy.random import *



os.environ['SAGE_ROOT'] = '/usr/share/sagemath'
os.environ['DOT_SAGE']='/home/mpetrec/.sage'
#os.environ['SAGE_SRC'] = 'you can find this path by running os.environ['SAGE_ROOT'] in sage shell 
#os.environ['SAGE_DOC_SRC'] = 'you can find this path by running os.environ['SAGE_ROOT'] in sage shell 
os.environ['SAGE_LOCAL'] = '/usr'
#os.environ['DOT_SAGE'] = 'you can find this path by running os.environ['SAGE_ROOT'] in sage shell
import sarx_to_switchedPol

import sage.all
from sage.rings.real_mpfr  import RealField
from sage.rings.rational_field  import RationalField
from sage.rings.polynomial.multi_polynomial_ideal import MPolynomialIdeal
from sage.numerical.optimize import minimize

def general_scalar_input(time, input_sequence):
	ctime = 0
	uindex = 0
	while ctime <= time and uindex < len(input_sequence):
		ctime = input_sequence[uindex][0]
		uindex = uindex + 1
	
	return input_sequence[uindex-1][1]

def generate_random_input_sequence(interval, step_number, mean,\
                             covariance):
	timeaxis = linspace(0,interval,step_number)
	seed()
	input_sequence=[]
	for index in range(step_number):
		input_value=normal(mean,covariance)
		input_sequence.append((timeaxis[index],input_value))

	return input_sequence	


	switched_systems.def_precision = 1e-2

def generateSARX(params):
	disc_modes=[(1,1),(1,2),(2,1),(2,2)]
	i_reg_length=2
	o_reg_length=2

	reg_param=dict([])

	seed()
	pvalue=[0.3,0.7]
	vvalue=[2,5]


	for dstate in disc_modes:
		print("Dstate:"+str(dstate[0])+"\n")
		pval=pvalue[dstate[0]-1]
		vval=vvalue[dstate[1]-1]

		reg_list=[]
		for i in range(4):
			param=params[[i,4+i,8+i,12+i,16+i]]
			reg_list.append(asscalar(dot(param,array([0.001,pval,pval*pval, vval,vval*vval],float))))
	
		reg_param[dstate]=reshape(array(reg_list,float),(1,4))
		

	sarx = sarx_to_switched.SARX(discrete_modes=disc_modes,output_reg_length=o_reg_length,input_reg_length=i_reg_length,regression_parameters=reg_param)

	print "SARX: \n"+ "PARAM: "+str(params)+"\n"
	sarx.ownprint()
	
	return sarx


def generate_Reg():
	disc_modes=[(1,1),(1,2),(2,1),(2,2)]
	i_reg_length=2
	o_reg_length=2


	seed()
	pvalue=[0.3,0.7]
	vvalue=[2,5]

#	pvalue=pvalue.flatten()
#	vvalue=vvalue.flatten()

	reg=[]
	for dstate in disc_modes:
		print("Dstate:"+str(dstate[0])+"\n")
		pval=pvalue[dstate[0]-1]
		vval=vvalue[dstate[1]-1]

		reg.append([0.001,pval,pval*pval, vval,vval*vval])

	return transpose(array(reg,float))


def generate_PolReg(Theta):
	disc_modes=[(1,1),(1,2),(2,1),(2,2)]
	i_reg_length=2
	o_reg_length=2


	seed()
	pvalue=[0.3,0.7]
	vvalue=[2,4]

	reg=dict([])
	for dstate in disc_modes:
		print("Dstate:"+str(dstate[0])+"\n")
		pval=pvalue[dstate[0]-1]
		vval=vvalue[dstate[1]-1]
	
		reg_list=[]	
		for i in range(4):
			reg_list.append(0.001*Theta[i]+pval*Theta[i+4]+pval*pval*Theta[i+8]+vval*Theta[i+12]+Theta[16+i]*vval*vval)

		reg[dstate]=reg_list		

	return reg


	






switched_systems.init_module(switched_systems.def_precision)



reg=generate_Reg()

print "Regressor: "+str(reg)+"\n"


reg_des=transpose(array( [[8,-15,1,-3],[1,2,1,1],[8,-15,1,-3],[1,2,1,1]],float))

print "parameters_to_achieve: "+str(reg_des)+"\n"

ret=linalg.lstsq(transpose(reg),transpose(reg_des),rcond=-1)


print " test: "+str(dot(reg, transpose(array([1,-1,-1,1],float))))+"\n"

print "LS: "+str(ret)+"\n"

z=transpose(ret[0])

print " parameters "+str(z)+"\n"
print " flat parameters "+array2string(z.flatten('F'), formatter={'float_kind':lambda x: "%.4f" % x}, separator=' & ')+"\n"

print " check: "+ str(dot(z,reg)-reg_des)+"\n"


sarx=generateSARX(z.flatten('F'))



linsys = sarx.SARX2LSS()


print "Linsys \n "
linsys.ownprint ()

print "Is Obseravble \n"+ str(linsys.IsObservable())+\
      " Is Reachable \n" + str(linsys.IsReachable())+"\n"

mlinsys = linsys.MinimalSystem()

repr = linsys.ComputeRepresentation()

mrepr = repr.MinimalRepresentation()

mdim = mrepr.dimension

print "Minimal representation: \n"
mrepr.ownprint()

print "Minimal dimension :"+str(mdim)+"\n"
print "Minimal system \n"
mlinsys.ownprint()

print("Is polynomial condition true: "+str(sarx.testPolynomials())+"\n")

#pol1=poly([1+1j,1-1j,1])
#pol2=poly([2,1+2j,1-2j])

#print ("Is comprime: "+str(sarx_to_switched.poly_coprime(pol1,pol2))+"\n")

RealRR=RealField(8)

PP=sage.rings.polynomial.polynomial_ring_constructor.PolynomialRing(RationalField(),3,'theta')


Thetas=PP.gens()


ThetasList=[Thetas[0],Thetas[1]+Thetas[0]]
for i in range(2,20):
		ThetasList.append((Thetas[0]-Thetas[1])**(i%2))
ThetasList[19]=ThetasList[19]+1

regs=generate_PolReg(ThetasList)

print "regs polynomials: "+str(regs)+"\n"



disc_modes=[(1,1),(1,2),(2,1),(2,2)]
i_reg_length=2
o_reg_length=2


sarxPoly = sarx_to_switchedPol.SARXPoly(discrete_modes=disc_modes,output_reg_length=o_reg_length,input_reg_length=i_reg_length,regression_parameters=regs,PolyRing=PP)

sarxPoly.ownprint()

pp=sarxPoly.testPolynomials()

print "Resulting ideal: "+str(pp)+"\n"

#sys.exit(-1)

poly=0

for p in pp:
	poly=poly+p*p

print "Polynomial: "+str(poly)+"\n"


#sol=minimize(poly_func,ThetaSubs)

#print "Solution: "+str(sol)+"\n"

pp4=sarxPoly.SARX2LSSMin()



print "Det. obs. matrix: "+str(pp4)+"\n"
print "Number of generetors: "+str(len(pp4.gens()))+"\n"
print "Calculating Grobner basis: \n"+str(pp4.groebner_basis())+"\n"

#sys.exit(-1)

ThetaSubs=[0]
for i in range(1,20):
		ThetaSubs.append(0**(i%2))

ThetaSubs[19]=ThetaSubs[19]+1



print "Parameters: "+str(ThetaSubs)+"\n"




sarx2=generateSARX(array(ThetaSubs))

linsys2 = sarx2.SARX2LSS()


print "Linsys \n "
linsys2.ownprint ()

print "Is Obseravble \n"+ str(linsys2.IsObservable())+\
      " Is Reachable \n" + str(linsys2.IsReachable())+"\n"

print("Is polynomial condition true: "+str(sarx2.testPolynomials())+"\n")






