from form_pow_repr import *
from word_operations import *
from numpy import *
from numpy.linalg import *
import copy

from own_io import own_print

is_output=False
#is_output = True

def_precision = 1e-10

	

#Class representing Hankel matrices
class HankelMatrix:
	def __init__(self,alphabet,odimension,matrix,index_set,length ):
 	#	self.dict_alphabet = SetToDict( alphabet )
		self.length = length
		self.alphabet = alphabet
		self.odimension = odimension 
	#	self.dict_odimension = SetToDict( range( odimension )) 
		self.matrix = matrix
		self.index = index_set
	#	self.dict_index = SetToDict( index_set )

	def ownprint(self):
		#print "Dict_alphabet:"+str(self.dict_alphabet)+"\n"
		print "Lenght: "+str(self.length)+"\n"
		print "Alphabet: "+str(self.alphabet)+"\n"
		print "Odimension:"+str(self.odimension)+"\n"
		#print "Dict_Odimension"+str(self.dict_odimension)+"\n"
		print "Matrix: "+str(self.matrix)+"\n"
		print "Index:"+str(self.index)+"\n"
		#print "Dict_index:"+str(self.dict_index)+"\n"

	#Returns a column of the hankel matrix indexed by the word w		
	def GetColumn(self, word, index,matrix=[]):
		if matrix==[]:
			matrix=self.matrix
		number=IndexToNumber(
		  self.alphabet, self.index, word,index)
		return reshape(matrix[:,number],(matrix.shape[0],1))
	
	#Returns a row  of the hankel matrix indexed by the word w		
	def GetRow(self, word, index, matrix=[]):
		if matrix==[]:
			matrix=self.matrix
		number=IndexToNumber(
		  self.alphabet, range(self.odimension), word,index)
		own_print(is_output,"RowNumber:"+str(number)+"\n"  )
		return reshape(matrix[number,:], (1,matrix.shape[1]))
	
        #Returns the element of the Hankel matrix indexed by row index rword and by column index cword
	def GetElement(self, rword, rindex, cword, cindex):
		rnumber=IndexToNumber(
		  self.alphabet, range(self.odimension), rword, rindex)
		cnumber=IndexToNumber(
		  self.alphabet, self.index, cword,cindex)

		return self.matrix[rnumber,cnumber]


        #If OR is the decomposition of H_{N,N+1}, then the function below returns R_q: the column of R_q indexed by (w,j) equals the column of R indexed by (wq,j)
	def ComputeRHC(self, matrix, letter):
		(n,M) = shape(matrix)

 		l = len(self.index)

		d = len(self.alphabet) 		


		if (d == 1):
			Mn = M - l
		else:
			Mn = (M/(d*l))*l


		
		new_matrix_index = []
		for i in range(Mn):
			ind = i % l

			vind = i / l

			new_ind = (vind * d + self.alphabet.index(letter)+1)*l+ind

			new_matrix_index.append(new_ind)


		RHC = matrix[:,new_matrix_index]

		return RHC



 
        #If OR is the decomposition of H_{N,N+1}, then the function below returns barR consisting of all the columns of R indexed by (w,j), |w| <= N
	def ComputeLHC(self, matrix):
		(n,M) = shape(matrix)

 		l = len(self.index)

		d = len(self.alphabet) 		


		if (d == 1):
			Mn = M - l
		else:
			Mn = (M/(d*l))*l


		
		LHC = matrix[:,range(Mn)]

		return LHC


	




        #If OR is the decomposition of H_{N+1,N}, then the function below returns O_q: the row of O_q indexed by (w,j) equals the row of R indexed by (qw,j)
	def ComputeRH(self, matrix, letter):

 		l = self.odimension

		d = len(self.alphabet) 		



		length = self.length


		
		new_matrix_index = []
		index_old = -1;
		for i in range(length+1):
			for j in range(d**i):
				new_wind = (self.alphabet.index(letter)+1)*(d**i)+index_old
				for ind in range(self.odimension):

					new_ind = (new_wind+1)*l+ind

					new_matrix_index.append(new_ind)

				index_old = index_old + 1


		RH = matrix[new_matrix_index,:]

		return RH
	




        #If OR is the decomposition of H_{N+1,N}, then the function below returns barO consisting of all the rows of O indexed by (w,j), |w| <= N
	def ComputeLH(self, matrix):	
		(M,n) = shape(matrix)

 		l = self.odimension

		d = len(self.alphabet) 		


		if (d == 1):
			Mn = M - l
		else:
			Mn = (M/(d*l))*l


		
		LHC = matrix[range(Mn),:]

		return LHC


		
	def GetSingularValues(self):
			
		svd_decomp = compute_own_svd(self.matrix, \
		                             cutoff = new_def_precision)
		#is_output = False
		
		if svd_decomp == "No solution":
			print "No solution to the decomp\n"
			S = array([[0]])
			U = zeros((self.matrix.shape[0],1))
			VT = zeros((1,self.matrix.shape[1]))
		else:		
			U=svd_decomp[0]
			S=svd_decomp[1]
			VT=svd_decomp[2]
	
		own_print(True, "SVD:"+str(S)+"\n")
		
		return S

 
        #Compute a representation from the Hankel matrix H_{N+1,L} (if is_row = True) or from H_{L,N+1} (if is_row=False)). If hankel is the Hankel-matrix object, then 
        # hankel.ComputeRepresentation() works under the assumption that hankel is a Hankel-matrix of the form H_{N+1,L} (if is_row=True) or under the assumption that
        # hankel is a Hankel matrix of the form H_{K,N+1} (if is_row=False). 		
	def ComputeRepresentation(self, new_def_precision=def_precision,is_row = True):
		svd_decomp = compute_own_svd(self.matrix, \
		                             cutoff = new_def_precision) #performs SVD decomposition
		
		own_print(is_output,\
		   "Def_precision :"+str(def_precision)+"\n")    

		if svd_decomp == "No solution":
			print "No solution to the decomp\n"
			S = array([[0]])
			U = zeros((self.matrix.shape[0],1))
			VT = zeros((1,self.matrix.shape[1]))
		else:		
			U=svd_decomp[0]
			S=svd_decomp[1]
			VT=svd_decomp[2]
	
		own_print(is_output, "SVD:"+str(S)+"\n")
		
		O = matrixmultiply(U, sqrt(S))
		R = matrixmultiply(sqrt(S),VT)
		own_print(is_output,"O:"+str(O)+"R: "+str(R)+\
		 "OR:" +str(matrixmultiply(O,R))+"\n")
	
		#NEW_MATRIX = matrixmultiply(O,R)

		#(NEW_U,NEW_S,NEW_V) = compute_own_svd(NEW_MATRIX)

		#own_print(is_output, "SVD_NEW:"+array2string(NEW_S)+"\n")
		
		
		is_empty = True
		output = O[range(self.odimension),:]

        	zeta = dict()
		for i in self.index:
			zeta[i] = self.GetColumn([],i,R)

   		transition = dict()	
		if is_row:
			LH = self.ComputeLH(O)
		else:
			LH = self.ComputeLHC(R)

		
		(rank,garbage) = compute_rank(LH)

		own_print(is_output, "LH rank:"+str(rank)+"\n")

		for letter in self.alphabet:
			own_print(is_output,"LH:"+str(LH)+"\n")
			
			if is_row:
				RH = self.ComputeRH( O, letter )
				retval = linear_least_squares(LH, RH,def_precision)
				transition[letter] = retval[0]
				
			else:
				RH = self.ComputeRHC(R,letter)
				retval = linear_least_squares(transpose(LH), transpose(RH),def_precision)
				transition[letter] = transpose(retval[0])

			
			own_print(is_output,"RH: "+str(RH)+"\n")
			own_print(is_output,"Letter:"+str(letter)+"\n")



			if is_row:
				own_print(is_output,\
				"Retval0 :"+array2string(retval[0], \
				suppress_small=1)+\
				" Cost function: "+array2string(\
				   retval[1], suppress_small=1)+\
				 "Rank of LH:"+str(retval[2])+\
				 "Singular values of LH:"+\
				 array2string(retval[3], suppress_small=1)+\
	                         "\n" + " difference:"+\
        	                 array2string((RH-matrixmultiply(LH,transition[letter])), suppress_small=1)+ "\n")
				own_print(is_output,"Trans:"+str((matrixmultiply(LH,transition[letter])-RH))+"\n")
			else:
				own_print(is_output,\
				"Retval0 :"+array2string(retval[0], \
				suppress_small=1)+\
				" Cost function: "+array2string(\
				   retval[1], suppress_small=1)+\
				 "Rank of LH:"+str(retval[2])+\
				 "Singular values of LH:"+\
				 array2string(retval[3], suppress_small=1)+\
	                         "\n" + " difference:"+\
        	                 array2string((RH-matrixmultiply(transition[letter],LH)), suppress_small=1)+\
				 "\n")
				own_print(is_output,"Trans:"+str((matrixmultiply(transition[letter],LH)-RH))+"\n")

		repr = Representation(self.alphabet, transition, output, zeta)

		return repr
		
