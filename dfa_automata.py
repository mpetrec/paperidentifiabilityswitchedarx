from numpy import *
#from Numeric import *
from automata import *
from form_pow_repr import *
import ConfigParser


class BaseDFA(MooreAutomata):
	def __init__(self, alphabet, states,\
			transition, accepting_states_set,\
			zeta):
		output = dict()	
		
		for state in states:
			output[state] = 0 
			
		index = 1
		for state_set in accepting_states_set:
			for state in state_set:
				output[state] =  index
				
			index = index + 1
			
		MooreAutomata.__init__(self,alphabet = alphabet, state= states,\
		                       transition = transition,\
				       output = output,\
				       zeta = zeta)
		
		self.output_number = index 
	
	def BaseDFA2Representation( self, odimension  ):
  		state_dimension  = len( self.state )
		output_dimension = self.output_number + odimension
		
		dstates = dict()
		
		index = 0
		for state in self.state:
			dstates[state] = index
			#print "State: " + str(state)+\
			#       "Index: " + str(index)+"\n"
			index = index + 1
			
		transition_matrices = dict()
		for letter in self.alphabet:
			transition_matrices[letter]=zeros((state_dimension, state_dimension ), float)	
		
		
			for state in self.state:
				stateo = self.transition[(letter, state)]
				transition_matrices[letter][\
				              dstates[stateo], \
			                      dstates[state]] = 1

					      
		output_matrix = zeros( (output_dimension,\
		                            state_dimension ))
					    #, Float)			      
		for state in self.state:
			oindex = self.output[state]
			#print "Oindex:"+str(oindex)+\
			#      "State:"+str(state)+\
			#      "Index:"+str(dstates[state])+"\n"
			if oindex > 0: 
				srange = (oindex-1)*odimension
				erange = srange + odimension 
			#	print "Srange: "+str(srange)+\
			#	      "erange: "+str(erange)+\
			#	      "states:"+str(dstates[state])\
			#	      +"odimension: "+str(odimension)+"\n"
				#output_matrix[1:2,0]=ones((1,1))      
			#	print "Output:"+\
			#	str(output_matrix[srange:erange,dstates[state]])+\
				#"Ones:"+str(ones((1,1)))+"\n"
				output_matrix[srange:erange,\
				        dstates[state]
			        ]=\
				ones(odimension) #,Float)
				#print "Oindex: "+str(oindex)+\
				#  "Matrix:"+ str(output_matrix[srange:erange,dstates[state]])+"\n"
		
		rzeta = dict()
		for (key,state) in self.zeta.iteritems():
			rzeta[key] = zeros(state_dimension) #, Float)	
			rzeta[key][dstates[state]] = 1
						
		repr = Representation(self.alphabet, \
			  transition_matrices, output_matrix,
			  rzeta)
		
		return repr
			
class LinSwitchDFA(BaseDFA):

	LinSwitchDFA_attributes = ["alphabet", "state",\
	                 "transition", "accepting_state",\
			 "initial_state", "input_dimension",\
			 "output_dimension", "keys"]
	def __init__ (self, **arguments):
		
		input_data = dict()
		if arguments.has_key("config_file"):
			fp = arguments["config_file"]
			confp = ConfigParser.ConfigParser()

			confp.readfp(fp)

			input_data_list = confp.items("LinSwitchDFA")
			
			for element in input_data_list:
				key = element[0]
				value = element[1]
				input_data[key]=eval(value)
		else:
			input_data = arguments
			
		self.input_dimension  = input_data["input_dimension"]
		self.output_dimension = input_data["output_dimension"]
		
		zeta = dict()

		for key in input_data["keys"]:
			zeta[key] = input_data["initial_state"]
		
		accepting_states_set = []
	 	for letter in input_data["alphabet"]:
			for index in range(self.input_dimension):
				zeta[(letter,index)] = \
				 input_data["transition"][(letter, \
				       input_data["initial_state"])]
			
			lstates_set = []

			for state in input_data["states"]:
				if input_data["transition"][(letter,state)] in \
				   input_data["accepting_states"]:
				   	lstates_set.append(state)
			
			#if len(lstates_set) > 0 : 	
			accepting_states_set.append(\
				                    lstates_set)
		BaseDFA.__init__(self,input_data["alphabet"], \
		        input_data["states"],\
			input_data["transition"],\
		        accepting_states_set, zeta)
	
	def DFA2Representation(self):
		return BaseDFA.BaseDFA2Representation(self,\
		                            self.output_dimension)
		
	
