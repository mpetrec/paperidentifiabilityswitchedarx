
from word_operations import *

class HankelTableGenerator:
	attributes = ["size_function", \
	              "transition_function", "output_function",\
		      "index_set", "alphabet",\
		      "identity_map", "composition_operator", \
		      "output_range" ]

	def __init__(self, **arguments):
		for field in HankelTableGenerator.attributes:
			self.__dict__[field] = \
			    arguments[field]
	
	
	def  UpdateTransitionList(self, transition_list,\
	            start_index, index_range):
				    
		for index in range(index_range): 
			for letter in self.alphabet:
				funct1 = transition_list[\
			            start_index + index ]
			
				funct2 = self.transition_function[\
			              letter ]

				funct  = self.composition_operator(\
			             funct2, funct1)
				
				transition_list.append(funct)
		
		return
	
	def UpdateOutputList(self, transition_list, \
	     output_list, start_index, index_range):
	    
		for index in range(index_range):
	    		funct = transition_list[\
			          start_index + index ]
			output_element = dict()
			for init_state_index in self.index_set:
				for output_index in \
				     self.output_range:
					output_element[\
					 (init_state_index, \
					  output_index )] =\
			   		self.output_function(\
					funct, \
			        	init_state_index,\
					output_index )
			
			output_list.append(output_element)
		return			
			    

	def UpdateLists(self, output_list, \
	           transition_list, size):
		start_index = len(transition_list) - \
		       len(self.alphabet)**(size-1)
		
		index_range = len(self.alphabet)**(size-1)
		
		start_output_index = len(transition_list)

		output_index_range = len(self.alphabet)**size
		
		self.UpdateTransitionList(\
		        transition_list, start_index, \
			index_range )
		
		self.UpdateOutputList(transition_list,\
		        output_list, start_output_index, \
			output_index_range )
		
		return

		
	
	
	def ComputeColumn( self,cword, column, output_list,\
	                     index, size):
		if size == 0:
			for oindex in self.output_range:
				column.append(\
				 output_list[cword+1][\
				          (index, oindex)])
			
		else:
			for nletter in range(len(self.alphabet)):
				ncword = (cword + 1)*\
				     len(self.alphabet)+nletter
				self.ComputeColumn( ncword, \
				 column, output_list, index, size-1)
				 
		return			
	
	def UpdateColumnList( self, column_list, size):
		if size == 0:
			column_list.append(-1)
			return

		start_index = len(column_list) -\
		              len(self.alphabet)**(size - 1)
		
		index_range = len(self.alphabet)**(size - 1)

		for index in range(index_range):
			oword = column_list[\
				  start_index + index ]
			for nletter in range(len(self.alphabet)):
				
				nword = (oword+1)*\
				    len(self.alphabet) + nletter


	
				column_list.append(nword)
		
		return
		
		
	def ComputeSubHankelTable( self,\
	                       transition_list,\
			       output_list,\
	                          column_list, table, size ):
				  
		if size == 0:
			self.UpdateLists(output_list, \
			               transition_list, 1)
		else:	
			start_size = size + size - 1
			self.UpdateLists(output_list, \
			                  transition_list,\
		                               start_size+ 1)
			self.UpdateLists(output_list, \
			                transition_list, \
		                               start_size+ 2)
		
		length_old = len(column_list)
		column_list_old = column_list[0:length_old]

		self.UpdateColumnList(column_list, size)
		
		newcolumns = column_list[\
		                length_old:len(column_list)]
		
		for column_word in newcolumns:
			for key in self.index_set:
				column = []
				for snum in range(size+2):
					self.ComputeColumn(\
						column_word,\
						column,
						output_list,\
						key, snum)
				
				table.append(column)
			
		col_index = 0	
		for i in range(length_old):
			for index in self.index_set:
				self.ComputeColumn(\
				 column_list[i], \
				 table[col_index], output_list,\
				 index, size + 1)
		
				col_index = col_index + 1
		
		return
		
		
	def GetSqSubList(self, hankel_list, size ):
		word_length = len(self.alphabet)**(size+1)
		row_length = word_length * len(self.output_range)
		#print "Row_length:"+str(row_length)+" size:"+\
		#      str(size)+"\n"
		sub_hankel_list = []
		for col in hankel_list:
			sub_col = col[0:(len(col) - row_length)+1]
			sub_hankel_list.append(sub_col)
		
		return sub_hankel_list
	
	def GetDimension(self, table, size):
		sub_hankel_list = self.GetSqSubList(table, size)
		table_size = self.size_function(sub_hankel_list)
		return table_size
	

	def GetSubList(self,table, size_c, index_set):
		   
               	wordlist = WordGenerate(self.alphabet, size_c)
 			

		result_list=[]
		end_index = 0
		for word in wordlist:
			for index in range(len(index_set)):
				result_list.append(table[end_index])
				end_index = end_index + 1


		return (result_list, end_index)
			

			
	def GetSubHankel(self, table,size_c,size_r):
		

                (column_list, end_index)  = self.GetSubList(table,size_c, self.index_set)

		result_list = []

		for column in column_list:
			(trunc_column, end_index) = self.GetSubList(column, size_r, self.output_range)

			result_list.append(trunc_column)


		return result_list
		

	def HankelTable(self, length, size):
		output_list = []
		
		transition_list = [self.identity_map]
		self.UpdateOutputList(transition_list,\
		              output_list, 0,1)


		table = []

		table_length = 0

		column_list = []

		for snum in range(length+1):
			table_length = snum
			self.ComputeSubHankelTable(\
			   transition_list,\
			   output_list, \
			   column_list, table,\
			   snum)
			
			#print "hankel_list:"+str(table)+"\n"
			#print "hankel_list iter"+str(snum)+"rank: "+str(self.GetDimension(table,snum))+"\n"
			if self.GetDimension(table, snum) == size:
				break
		
		
		
		return (table, table_length)
	
	def ExtendColumns(self, outputs, output_list, \
	                       index_range,size):	
			       
		for inum in range(len(outputs)):
			column = outputs[inum]
			index  = index_range[inum]
			self.ComputeColumn(-1, column, \
			   output_list, index, size)
		
		return
	
	def ComputeMarkov(self, length, index_range):
			
		output_list     = []	
		transition_list = [ self.identity_map ]

		self.UpdateOutputList(transition_list,\
		              output_list, 0,1)
		
		outputs =[]
		for index in index_range:
			outputs.append([])
		
		self.ExtendColumns(outputs, output_list, \
		index_range, 0)

		for snum in range(length):
			self.UpdateLists(output_list,\
			            transition_list, snum+1)
			self.ExtendColumns(outputs, output_list, \
		               index_range, snum+1)
				    
		return outputs			    
