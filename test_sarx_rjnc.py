#!/usr/bin/env python
from numpy import *
from sets import *
import switched_systems
import sys,os
import copy
import Gnuplot
import sarx_to_switched
from numpy.random import *


def general_scalar_input(time, input_sequence):
	ctime = 0
	uindex = 0
	while ctime <= time and uindex < len(input_sequence):
		ctime = input_sequence[uindex][0]
		uindex = uindex + 1
	
	return input_sequence[uindex-1][1]

def generate_random_input_sequence(interval, step_number, mean,\
                             covariance):
	timeaxis = linspace(0,interval,step_number)
	seed()
	input_sequence=[]
	for index in range(step_number):
		input_value=normal(mean,covariance)
		input_sequence.append((timeaxis[index],input_value))

	return input_sequence	


	switched_systems.def_precision = 1e-2

switched_systems.init_module(switched_systems.def_precision)

disc_modes=[(1,1),(1,2),(2,1),(2,2)]
i_reg_length=2
o_reg_length=2

reg_param=dict([])

seed()
pvalue=[0.3,0.7]
vvalue=[2,5]


for dstate in disc_modes:
	print("Dstate:"+str(dstate[0])+"\n")
	pval=pvalue[dstate[0]-1]
	vval=vvalue[dstate[1]-1]

	reg_list=[]
	for i in range(4):
		param=normal(0,1,(1,5))
		reg_list.append(asscalar(dot(param,array([0.001,pval,pval*pval, vval,vval*vval],float))))
	
	reg_param[dstate]=reshape(array(reg_list,float),(1,4))




sarx = sarx_to_switched.SARX(discrete_modes=disc_modes,output_reg_length=o_reg_length,input_reg_length=i_reg_length,regression_parameters=reg_param)

print "SARX: \n"
sarx.ownprint()


linsys = sarx.SARX2LSS()


print "Linsys \n "
linsys.ownprint ()

print "Is Obseravble \n"+ str(linsys.IsObservable())+\
      " Is Reachable \n" + str(linsys.IsReachable())+"\n"

mlinsys = linsys.MinimalSystem()

repr = linsys.ComputeRepresentation()

mrepr = repr.MinimalRepresentation()

mdim = mrepr.dimension

print "Minimal representation: \n"
mrepr.ownprint()

print "Minimal dimension :"+str(mdim)+"\n"
print "Minimal system \n"
mlinsys.ownprint()

print("Is polynomial condition true: "+str(sarx.testPolynomials())+"\n")

#pol1=poly([1+1j,1-1j,1])
#pol2=poly([2,1+2j,1-2j])

#print ("Is comprime: "+str(sarx_to_switched.poly_coprime(pol1,pol2))+"\n")


